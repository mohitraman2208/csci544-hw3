__author__ = 'raj'

import sys

classes=['it\'s','its','you\'re','your','they\'re','their','loose','lose','to','too']
count=0
correct=0
dev = sys.argv[1]
err = sys.argv[2]
res = sys.argv[3]
total_changes = 0
required_changes = 0
with open(dev,"r") as a,open(err,"r") as b,open(res,"r") as c:
    for x,y,z in zip(a,b,c):
        x=x.strip()
        y=y.strip()
        z=z.strip()
        x=x.split()
        y=y.split()
        z=z.split()
        for i in range(len(x)):
            if(y[i].strip()!=x[i].strip()):
                required_changes+=1
                if(x[i].strip()==z[i].strip()):
                    correct+=1
            if y[i].strip()!=z[i].strip():
                total_changes+=1

precision = correct/total_changes
recall = correct/required_changes
print(correct)
print(count)
print("Acc" + str(correct/total_changes))
fscore = (2*precision*recall)/(precision+recall)

print("fscore:" + str(fscore))