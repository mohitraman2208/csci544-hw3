__author__ = 'mohit'

import sys
import os
import json
import re
from perceptron import Perceptron

SENTENCE_START_CONST = 'sent_Start'
SENTENCE_END_CONST = 'sent_End'

WORD_LABELS = [ 'wprev:', 'wcurr:', 'wnext:']
POS_LABELS = ['pos1:','pos2:']
SUFFIX_LABELS = ['suffix2:','suffix3:']

def getWordFeatures(word):

    suffix_features = ''
    suffix_features += SUFFIX_LABELS[0]+word[len(word)-2:] if len(word) > 1 else SUFFIX_LABELS[0]+'0'
    suffix_features += ' '+SUFFIX_LABELS[1]+word[len(word)-3:] if len(word) > 2 else ' '+SUFFIX_LABELS[1]+'0'
    return suffix_features

def getfeatures(words, index, pos_predictions):

    words_features = ''
    delim = ''
    for w in range(len(WORD_LABELS)):
        i = index+w-1
        if (i >= 0) and (i < len(words)):
            mytemp = words[i]
        elif i < 0:
            mytemp = SENTENCE_START_CONST
        else:
            mytemp = SENTENCE_END_CONST
        words_features += delim + WORD_LABELS[w]+mytemp
        delim = ' '

    pos_features = ''
    for w in range(len(POS_LABELS)):
        temp = pos_predictions[w-2] if (len(pos_predictions) - (len(POS_LABELS) - w -1) > 0) \
            else SENTENCE_START_CONST
        pos_features += delim + POS_LABELS[w]+temp

    return words_features+pos_features

# def create_pos_features(line):
#
#     word_dict = {}
#     words = line.split()
#
#     prev_word = PREV_WORD_LABEL_CONST+SENTENCE_START_CONST
#
#     for index in range(len(words)):
#
#         next_word = NEXT_WORD_LABEL_CONST+words[index+1] \
#             if index < len(words) - 1 else NEXT_WORD_LABEL_CONST+SENTENCE_END_CONST
#
#         newLine = prev_word+' '+CURR_WORD_LABEL_CONST+words[index]+' '+next_word
#
#         word_dict[index] = newLine
#
#         prev_word = PREV_WORD_LABEL_CONST+words[index]
#
#     #print(word_dict)
#     return word_dict

def main(line,labeltoweight_dict):
    # This is the classifier for POS tagger
    # Takes model_file name as I/P
    # model_file contains Average Weight to be intialized for average Perceptron

    perceptron = Perceptron()
    
    tagged_output = ''
    
    # my_regex = re.compile(r'\b(they|you|it)[\s]*[ia\'][\s]*(re|s)\b',flags=re.IGNORECASE)
    if line:

        line = line.strip()
        
        # line = my_regex.sub('\\1 \'\\2',line)
        
        

        my_delim = ''

        pos_predictions = []
        words = line.split()
        for index in range(len(words)):

            features = getfeatures(words, index, pos_predictions)

            features += ' ' +getWordFeatures(words[index])

            feature_vector_dict = perceptron.create_feature_vector(features.split())

            tag = perceptron.percep_classify(labeltoweight_dict, feature_vector_dict)
            pos_predictions.append(tag)

            tagged_output += my_delim + words[index] + "/" + tag

            my_delim = ' '
    
    return tagged_output    

def init(mode_file_name):
    try:
        with open(mode_file_name,"r") as mode_file_handle:
            labeltoweight_dict = json.loads(mode_file_handle.read())
    except IOError:
        print("Error: Cannot open File")
        sys.exit(1)
    except TypeError:
        print("Error: Invalid JSON Model File")
        sys.exit(1)
    return labeltoweight_dict 

if __name__ == "__main__":
   
    if len(sys.argv) != 2 or sys.argv[1] == '' or not os.path.isfile(os.path.abspath(sys.argv[1])):
        print("Invalid Arguments!")
        exit(1)
    
    mode_file_name = sys.argv[1]
    labeltoweight_dict = init(mode_file_name)
    lines = sys.stdin.readlines()
    for line in lines:
        main(lines,labeltoweight_dict)