'''
Created on Mar 15, 2015

@author: Meenakshi
'''
import sys
import os
import json
import re
from perceptron import Perceptron
import call_postag

SENTENCE_START_CONST = 'sent_Start/sent_Start'
SENTENCE_END_CONST = 'sent_End/sent_End'
TEMP_FILE_CONST = '_temp'

WORD_LABELS = [ 'wp1:', 'wp2:','wc:','wn1:','wn2:']
POS_LABELS = ['pos1:','pos2:']

#  init_error_labels():
regex_replace = re.compile(r"\b(you|they|it)([^\s]*\s?)(\'re|\'s)",flags=re.IGNORECASE)

you_are_regex = re.compile(r"\byou\'re(\b|/)",flags=re.IGNORECASE)
your_regex = re.compile(r"\byour(\b|/)",flags=re.IGNORECASE)

they_are_regex = re.compile(r"\bthey\'re(\b|/)",flags=re.IGNORECASE)
their_regex = re.compile(r"\their(\b|/)",flags=re.IGNORECASE)

it_is_regex = re.compile(r"\bit\'s(\b|/)",flags=re.IGNORECASE)
its_regex = re.compile(r"\bits(\b|/)",flags=re.IGNORECASE)

lose_regex = re.compile(r"\blose(\b|/)",flags=re.IGNORECASE)
loose_regex = re.compile(r"\bloose(\b|/)",flags=re.IGNORECASE)

to_regex = re.compile(r"\bto(\b|/)",flags=re.IGNORECASE)
too_regex = re.compile(r"\btoo(\b|/)",flags=re.IGNORECASE)

'''
your_you_regex = re.compile(r"\byou(r|\'re)\b",flags=re.IGNORECASE)
to_too_regex = re.compile(r"\bt(o|oo)\b",flags=re.IGNORECASE)
its_it_regex = re.compile(r"\bit(s|\'s)\b",flags=re.IGNORECASE)
their_theyare_regex = re.compile(r"\bthe(y\'re|ir)\b",flags=re.IGNORECASE)
lose_loose_regex = re.compile(r"\blo(se|ose)\b",flags=re.IGNORECASE)
'''

# first_check = { 'youare/your': your_you_regex, 'theyare/their' : their_theyare_regex, 'itis/its' : its_it_regex, 'lose/loose' : lose_loose_regex, 'to/too' : to_too_regex }

label_to_regex = { 'youare':you_are_regex, 'your':your_regex, 'theyare' : they_are_regex,'their':their_regex, 'itis' : it_is_regex, 'its':its_regex, 'lose': lose_regex, 'loose' : loose_regex, 'to' : to_regex, 'too' : too_regex}

labels_to_wieghtVec = { 'youare' : 'youare/your', 'your' : 'youare/your', 'theyare' : 'theyare/their', 'their': 'theyare/their', 'itis' : 'itis/its','its':'itis/its', 'lose': 'lose/loose', 'loose' : 'lose/loose', 'to' : 'to/too', 'too':'to/too'}

labels_to_rev = {'youare' : 'r', 'your' : '\'re', 'theyare' : 'ir', 'their' : 'y\'re','itis':'s','its' :'\'s','lose' : 'ose', 'loose' : 'se', 'to' : 'o', 'too' : ''}

my_label_dict = {}


# init ends
def get_word_pos_asfeatures(words,index):
    
    words_features = ''
    delim = ''
    index = int(len(WORD_LABELS)/2) if index == -1 else index
    
    for w in range(len(WORD_LABELS)):
       
        i = index+w-2
        if index == i:
            continue
        elif (i < 0 or i >= len(words)) or ('/' not in words[i]):
            mytemp = SENTENCE_START_CONST if i < index else SENTENCE_END_CONST
        else:
            mytemp = words[i]
        words_features += delim + WORD_LABELS[w]+'/'.join(mytemp.split('/')[:-1])
        delim = ' '

    pos_features = ''
    for w in range(len(POS_LABELS)):
        i = index+w-len(POS_LABELS)
        if i < 0 or '/' not in words[i]:
            mytemp = SENTENCE_START_CONST
        else:
            mytemp = words[i]

        pos_features += delim + POS_LABELS[w]+mytemp.split('/')[-1]

    return words_features+pos_features

perceptron = Perceptron()


def reverse_word(currlabel, currentWord):
    
    change = labels_to_rev[currlabel]
    currentWord = re.sub('(you|it|to|the|lo)(.*)', '\\1',currentWord,flags=re.IGNORECASE)
    currentWord = currentWord + change
    return currentWord 


def contract_word(currlabel, currentWord):
    labels = labels_to_wieghtVec[currlabel].split('/')
    for all_label in labels:
        if all_label is not currlabel:
            change = labels_to_rev[all_label]
            currentWord = re.sub('(you|it|to|the|lo)(.*)', '\\1',currentWord,flags=re.IGNORECASE)
            currentWord = currentWord + change
        break
    
    return currentWord

def classify(currlabel, currentWord, new_line):
    
    global perceptron,my_label_dict    
    
    feature_vector_dict = perceptron.create_feature_vector(new_line.split())
    
    wght_vec = labels_to_wieghtVec[currlabel]
    
    labeltoweight_dict = my_label_dict[wght_vec]
            
    tag = perceptron.percep_classify(labeltoweight_dict, feature_vector_dict)
    
    if tag == currlabel:
        return currentWord
    
    return reverse_word(currlabel,currentWord)
    
currlabel = ''
sec_last = ''
my_last = ''

def classify_and_replace(matchobj):
    
    global sec_last, my_last, currlabel;
    
    my_groups = list(matchobj.groups())
                
    # edge cases when regex overlap TODO: Think of a better scalable way
    #edge case - two its should have 2 words in between
    if not my_groups[1]:
        my_groups[0] = sec_last
        my_groups[1] = my_last
    elif not my_groups[0]:
        my_groups[0] = my_last
    
    sec_last = my_groups[3]
    my_last = my_groups[4]
    new_line = get_word_pos_asfeatures(my_groups,-1)
    
    my_groups[2] = classify(currlabel, my_groups[2], new_line)
    
    return ' '.join(my_groups).strip()


def search_pos_index(regex_label, pos_index, pos_words):
    
    while pos_index < len(pos_words):
        if regex_label.match(pos_words[pos_index]) is not None:
            return pos_index
        pos_index += 1
    return -1

def extract_features(label, regex_label, line, pos_line, substitutions):

    if regex_label.search(line) is None:
        return line
    
    pos_line = regex_replace.sub(r'\1\3',pos_line)
    
    pos_words = pos_line.split()
    
    words = line.split()
    
    pos_index = -1
    for index in range(len(words)):
        if regex_label.match(words[index]) is not None:
            
            # pos_index = index if index > (pos_index + 1) else pos_index+1
            pos_index = search_pos_index(regex_label, pos_index + 1, pos_words)
            
            if pos_index == -1:
                raise RuntimeError
            new_line = get_word_pos_asfeatures(pos_words, pos_index)
            new_word = classify(label, words[index], new_line)
            if regex_label.match(new_word) is None:
                substitutions[index] = new_word
    
    '''
    if regex_label is you_are_regex or regex_label is they_are_regex or regex_label is it_is_regex:
        
        currlabel = label
    
        newline = regex_label.sub(classify_and_replace,line)
            
    else:
        words = line.split()
        for index in range(len(words)):
            if regex_label.match(words[index]) is not None:
                new_line = get_word_pos_asfeatures(words, index)
                words[index] = classify(label, words[index], new_line)
        newline = ' '.join(words).strip()
    '''


def perform_substitutions(line, substitutions):
    
    if substitutions:
        
        words = line.split()
        
        for key,value in substitutions.items():
            words[key] = value
        
        return " ".join(words).strip()
    
    return line   

def tag_file(line,labeltoweight_dict):
    substitutions = {}
    
    pos_line = re.sub( r'([a-zA-Z])([^\s\w])', r'\1 \2', line )
    pos_line = call_postag.main(pos_line,labeltoweight_dict)
    
    for label, regex_label in label_to_regex.items():
        
        extract_features(label,regex_label, line, pos_line,substitutions)
    
    return perform_substitutions(line,substitutions)


def main():
    # This is the classifier for Error Correction tagger
    # I/p - Takes model_file name as I/P
    # model_file contains Average Weight to be intialized for average Perceptron
    # I/p - test file
    
    global my_label_dict
    
    if len(sys.argv) != 3 or sys.argv[1] == '' or not os.path.isfile(os.path.abspath(sys.argv[1])) or sys.argv[2] == '':
        print("Invalid Arguments!")
        exit(1)

    # Command line argument 1 - Model file name

    mode_file_name = sys.argv[1]

    try:
        with open(mode_file_name,"r") as mode_file_handle:
            my_label_dict = json.loads(mode_file_handle.read())
    except IOError:
        print("Error: Cannot open File")
        sys.exit(1)
    except TypeError:
        print("Error: Invalid JSON Model File")
        sys.exit(1)

    test_file_name = sys.argv[2]
    labeltoweight_dict = call_postag.init("pos.model")
    with open(test_file_name, 'r') as test_file:
        for line in test_file:
    
            line = line.strip()
            output = ''
            if line:
                output = tag_file(line,labeltoweight_dict)
            
            print(output)
            sys.stdout.flush()

if __name__ == "__main__":
    main()
