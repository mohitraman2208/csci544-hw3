# README #

### Error detection and correction: dealing with homophone confusion - csci544-hw3 ###

* Approach
* Data
* Files
* Instructions to execute scripts
* Accuracy/Fscore
* Formatting/Handling Punctations and spaces

### Approach ###

Used averaged perceptron from homework 2 to perform binary classification for each possible homophone error type. 

* Training the Classifier

For each error type( ex: to/too), search for the possible values(like to or too) and extract features.

The features contain 2 words to the left, their POS tags and 2 words to the right of the correct label-

label_name wprev2: left_word_2 wprev1: left_word_1 wnext1: right_word_1 wnext2:right_word_2 pos1: pos_left_2

These features are passed to the Perceptron to create average weight vectors.

* Classifying errors/ Error correction - 

For each error type( ex: to/too), search for the possible values(like to or too) and extract features as above. These features are passed to the Perceptron to get the predicted error.

### Data ###

Training data comprises of 150000 lines of randomly selected lines from a kaggle dataset for sentiment analysis and wikipedia dump.

[ Kaggle Dataset link](https://kaggle2.blob.core.windows.net/competitions-data/kaggle/3927/train_v2.txt.zip?sv=2012-02-12&se=2015-03-16T22%3A52%3A37Z&sr=b&sp=r&sig=Hd%2FR82YsffrG1mFblWVt6k5%2BEXHD8Ms%2BFFDabtPU1OY%3D)

### Files ###

* hw3.output.txt - test data output
* src/eclearn.py - training program
* src/ectag.py - Classifier/Error corrector
* src/perceptron.py - Perceptron class file
* src/call_pos.tag - POS tagger

### Instructions to run code ###

* Training the Classifier

python3.4 eclearn.py train_file_name model_file_name

* Run Classifier

python3.4 ectag.py model_file_name test_file_name

Outputs the corrected sentences on STDOUT.

Note: This code requires pos.model file.


### Accuracy/F-score on Dev set ###

Accuracy: 0.9718085106382979

F-score : 0.979755999463735

### Formatting/Handling Punctations and spaces ###

To perform effective POS tagging the i/p sentences are formatted such that most punctuations are split from adjoining words.

O/P is not formatted and only corrections are made on the respective words.