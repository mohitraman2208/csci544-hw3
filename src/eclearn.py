from pyexpat import model
__author__ = 'mohit'

import sys
import os.path
import json
import re
from perceptron import Perceptron

SENTENCE_START_CONST = 'sent_Start/sent_Start'
SENTENCE_END_CONST = 'sent_End/sent_End'
TEMP_FILE_CONST = '_temp'

WORD_LABELS = [ 'wp1:', 'wp2:','wc:','wn1:','wn2:']
POS_LABELS = ['pos1:','pos2:']

#  init_error_labels():
you_are_regex = re.compile(r"([^\s\b]*)[\s]*([^\s]*)[\s]*\b(you/[^\s]+\s+\'re/[^\s]+)\b[\s]*([^\s]*)[\s]*([^\s\b]*)",flags=re.IGNORECASE)
your_regex = re.compile(r"\byour/[^\s]+\b",flags=re.IGNORECASE)

they_are_regex = re.compile(r"([^\s\b]*)[\s]*([^\s]*)[\s]*\b(they/[^\s]+\s+\'re/[^\s]+)\b[\s]*([^\s]*)[\s]*([^\s\b]*)",flags=re.IGNORECASE)
their_regex = re.compile(r"\btheir/[^\s]+\b",flags=re.IGNORECASE)

it_is_regex = re.compile(r"([^\s\b]*)[\s]*([^\s]*)[\s]*\b(it/[^\s]+\s+\'s/[^\s]+)\b[\s]*([^\s]*)[\s]*([^\s\b]*)",flags=re.IGNORECASE)
its_regex = re.compile(r"\bits/[^\s]+\b",flags=re.IGNORECASE)

lose_regex = re.compile(r"\blose/[^\s]+\b",flags=re.IGNORECASE)
loose_regex = re.compile(r"\bloose/[^\s]+\b",flags=re.IGNORECASE)

to_regex = re.compile(r"\bto/[^\s]+\b",flags=re.IGNORECASE)
too_regex = re.compile(r"\btoo/[^\s]+\b",flags=re.IGNORECASE)

label_to_regex = { 'youare':you_are_regex, 'your':your_regex, 'theyare' : they_are_regex,'their':their_regex, 'itis' : it_is_regex, 'its':its_regex, 'lose': lose_regex, 'loose' : loose_regex, 'to' : to_regex, 'too' : too_regex}

# label_to_regex = { 'youare':you_are_regex, 'your':your_regex, 'theyare' : they_are_regex,'their':their_regex, 'itis' : it_is_regex, 'its':its_regex, 'lose': lose_regex, 'loose' : loose_regex} #,'to' : to_regex, 'too' : too_regex}

labels_to_wieghtVec = { 'youare' : 'youare/your', 'your' : 'youare/your', 'theyare' : 'theyare/their', 'their': 'theyare/their', 'itis' : 'itis/its','its':'itis/its', 'lose': 'lose/loose', 'loose' : 'lose/loose', 'to' : 'to/too', 'too':'to/too'}

my_label_dict = {}

my_label_dict['youare/your'] = 'temp_1.txt'
my_label_dict['theyare/their'] = 'temp_2.txt'
my_label_dict['itis/its'] = 'temp_3.txt'
my_label_dict['lose/loose'] = 'temp_4.txt'
my_label_dict['to/too'] = 'temp_5.txt'

# init ends
def get_word_pos_asfeatures(words,index):
    
    words_features = ''
    delim = ''
    index = int(len(WORD_LABELS)/2) if index == -1 else index
    
    for w in range(len(WORD_LABELS)):
       
        i = index+w-2
        if index == i:
            continue
        elif (i < 0 or i >= len(words)) or ('/' not in words[i]):
            mytemp = SENTENCE_START_CONST if i < index else SENTENCE_END_CONST
        else:
            mytemp = words[i]
        words_features += delim + WORD_LABELS[w]+'/'.join(mytemp.split('/')[:-1])
        delim = ' '

    pos_features = ''
    for w in range(len(POS_LABELS)):
        i = index+w-len(POS_LABELS)
        if i < 0 or '/' not in words[i]:
            mytemp = SENTENCE_START_CONST
        else:
            mytemp = words[i]

        pos_features += delim + POS_LABELS[w]+mytemp.split('/')[-1]

    return words_features+pos_features



def print_features(label, regex_label, line,my_in_file):
    
    if regex_label.search(line) is None:
        return
    
    if regex_label is you_are_regex or regex_label is they_are_regex or regex_label is it_is_regex:
        sec_last = ''
        my_last = ''
        
        for m in regex_label.finditer(line):
            my_groups = list(m.groups())
            
            # edge cases when regex overlap TODO: Think of a better scalable way
            #edge case - two its should have 2 words in between
            if not my_groups[1]:
                my_groups[0] = sec_last
                my_groups[1] = my_last
            elif not my_groups[0]:
                my_groups[0] = my_last
            sec_last = my_groups[3]
            my_last = my_groups[4]
            new_line = label + ' ' +get_word_pos_asfeatures(my_groups,-1)
            wght_vec = labels_to_wieghtVec[label]
            my_in_file[wght_vec].write(new_line + '\n')
    else:
        words = line.split()
        for index in range(len(words)):
            if regex_label.match(words[index]) is not None:
                new_line = label + ' ' + get_word_pos_asfeatures(words, index)
                wght_vec = labels_to_wieghtVec[label]
                my_in_file[wght_vec].write(new_line + '\n')

def process_train_file(train_file_name,my_in_file):

    with open(os.path.abspath(train_file_name), 'r', errors='ignore') as train_file_handle: # with open(os.path.abspath('tst_input.txt'),'r',errors='ignore') as train_file_handle:

        for line in train_file_handle:
            line = line.strip()
            for label, regex_label in label_to_regex.items():
                print_features(label,regex_label, line,my_in_file)


def main():
    # This is the training program for error correction through classification
    # Take two command line arguments - training_file model_file
    # training_file contains training data and the learned model is stored in model file

    if len(sys.argv) != 3 or sys.argv[1] == '' or not os.path.isfile(os.path.abspath(sys.argv[1])) or sys.argv[2] == '':
        print("Invalid Arguments!")
        exit(1)

    # Command line argument 1 - Training file name
    train_file_name = sys.argv[1]
    
    '''
    my_in_file = {}
    for key,value in my_label_dict.items():
        my_in_file[key] = open(value,'w')
        
    process_train_file(train_file_name,my_in_file)
    
    '''
    # processed_file_content = process_train_file(train_file_name)
    # Command line argument 2 - Model file name
    model_file_name = sys.argv[2]
    
    percepLearn = Perceptron()
    
    model_dict = {}
    for key, training_lines in my_label_dict.items():
        print("Performing learn for " + key)
        percepLearn.init_percep_learn(training_lines, key.split('/'), 50)
    
        model = percepLearn.percep_learn()
        
        model_dict[key] = model
        
        print("Learn complete for " + key)
        
    with open(model_file_name,'w') as output_file:
        output_file.write(json.dumps(model_dict,indent=2))

    #os.remove(processed_train_file)
    # with open(model_file_name,'w') as output_file:
    #     output_file.write(json.dumps(model,indent=1))
if __name__ == "__main__":
    main()